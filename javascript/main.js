const llamandoFetch = (titulo) => {
  const url =
    "https://www.omdbapi.com/?t=" + titulo + "&plot=full&apikey=30063268";
  fetch(url)
    .then((response) => response.json())
    .then((data) => mostrarDatos(data))
    .catch((error) => {
      const lblError = document.getElementById("lnlError");
      alert("Error película no encontrada: "+error);
      limpiar();
    });
};

const mostrarDatos = (data) => {
    
  var buscar = document.getElementById("txtBuscar").value;
  var titulo = document.getElementById("txtTitulo");
  var fecha = document.getElementById("txtFecha");
  var actores = document.getElementById("txtActores");
  var texto = document.getElementById("txtPlot");
  var imagen = document.getElementById("imgPortada");   

  if(data.Title.toUpperCase() != buscar.toUpperCase()){
    alert("Error película no encontrada");
    limpiar();
  }else {
    titulo.value = data.Title;
    fecha.value = data.Year;
    actores.value = data.Actors;
    imagen.src = " ";
    imagen.src = data.Poster;

    texto.value = data.Plot;
  }
 
};

function limpiar() {
  var buscar = document.getElementById("txtBuscar");
  var titulo = document.getElementById("txtTitulo");
  var fecha = document.getElementById("txtFecha");
  var actores = document.getElementById("txtActores");
  var texto = document.getElementById("txtPlot");
  var imagen = document.getElementById("imgPortada");

  buscar.value = "";
  buscar.placeholder = "Ingresa el título a buscar";
  titulo.value = " ";
  fecha.value = " ";
  actores.value = " ";
  imagen.src = " ";
  texto.value = "La reseña aparecera aquí";
  
}

document.getElementById("btnBuscar").addEventListener("click", () => {
  let titulo = document.getElementById("txtBuscar").value;

  llamandoFetch(titulo);
});
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnIndex").addEventListener("click", function () {
  window.location.href = "../index.html";
});
